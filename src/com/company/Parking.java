package com.company;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Parking {
    Scanner scanner = new Scanner(System.in);

    int setCarPlaces() {
        while (true) {
            int carPlaces = scanner.nextInt();
            if (carPlaces >0) {
                return carPlaces;
            } else {
                System.out.println("Введите число положительное число!");
            }
        }
    }

    int setTruckPlaces() {
        while (true) {
            int truckPlaces = scanner.nextInt();
            if (truckPlaces >0) {
                return truckPlaces;
            } else {
                System.out.println("Введите положиельное число!");
            }
        }
    }

    ArrayList<Car> setParkingPlaces(int allPlaces) {
        ArrayList<Car> parkingPlaces = new ArrayList<>(allPlaces);
        for (int i = 0; i < allPlaces; i++) {
            parkingPlaces.add(i,new Car());
        }
        return parkingPlaces;
    }

    int setAmountOfTransportType(int places) {
        Random random = new Random();
        int amountOfCars = 0;
        int minimum = 0;
        int maximum = places / 3;
        while (amountOfCars == 0) {
            amountOfCars = random.nextInt((maximum - minimum)+2);//-чтобы не было проблем с 1,1
        }
        amountOfCars += minimum;
        return amountOfCars;
    }

    ArrayList<Car> createCars(int amountOfCars) {
        ArrayList<Car> carsList = new ArrayList<>(amountOfCars);
        for (int i = 0; i < amountOfCars; i++) {
            carsList.add(i,new Car(Randomaizer(10,1), Randomaizer(9999,1000)));
        }
        return carsList;
    }

    ArrayList<Truck> createTrucks(int amountOfTrucks) {
        ArrayList<Truck> trucksList = new ArrayList<>(amountOfTrucks);
        for (int i = 0; i < amountOfTrucks; i++) {
            trucksList.add(i,new Truck(Randomaizer(10,1), Randomaizer(9999,1000)));
        }
        return trucksList;
    }
    ArrayList<Car> deleteCars(ArrayList<Car> places) {
        for (int i = 0; i < places.size(); i++) {
            places.get(i).turns = 0;
            places.get(i).number = 0;
        }
        return places;
    }


    void toFindNearestFreePlace(ArrayList<Car> cars,int parkingPlaceforTrucks) {
        int minimumTurnCar = 11;
        int minimumTurnTruck = 11;
        for (int i = 0; i <parkingPlaceforTrucks ; i++) {
            if(cars.get(i).turns<minimumTurnTruck){
                minimumTurnTruck=cars.get(i).turns;
            }
        }
        for (int i = parkingPlaceforTrucks; i <cars.size() ; i++) {
            if(cars.get(i).turns<minimumTurnCar){
                minimumTurnCar=cars.get(i).turns;
            }
        }
        if(minimumTurnCar!=0 && minimumTurnTruck!=0){
            System.out.println("Ближайшее место на парковке машин освободиться через " + minimumTurnCar + " ходов,а я на грузовом через " + minimumTurnTruck + " ходов");

        }else if(minimumTurnTruck!=0){
            System.out.println("Ближайшее место на парковке грузовиков освободить через " + minimumTurnTruck + " ходов,а на парковке машин есть свободные места");
        }else if(minimumTurnCar!=0){
            System.out.println("Ближайшее место на парковке машин освободиться через " + minimumTurnCar + " ходов,а на парковке грузовиков есть свободные места");
        }else{
            System.out.println("На парковке машин и грузовиков есть свободные места");
        }
    }




    int checkFreePlaces(ArrayList<Car> places) {
        int freePlaces = 0;
        for (int i = 0; i < places.size(); i++) {
            if (places.get(i).turns == 0) {
                freePlaces++;
            }
        }
        return freePlaces;
    }
    int checkOccupiedPlaces (ArrayList<Car> places){
        int occupiedPlaces=0;
        for (int i = 0; i <places.size() ; i++) {
            if(places.get(i).turns!=0){
                occupiedPlaces++;
            }
        }
        return occupiedPlaces;
    }

    void toArriveTrucks(ArrayList<Truck> trucks) {
        if (trucks.size() == 0) {
            System.out.println("В этом ходу не приехало грузовиков ");
        }
        for (int i = 0; i < trucks.size(); i++) {
            System.out.println("Грузовик с номером  " + trucks.get(i).number + " приехал на парковку и будет стоять " + trucks.get(i).turns + "ход(ов)");
        }
    }
    void toArriveCars(ArrayList<Car> cars) {
        if (cars.size() == 0) {
            System.out.println("В этом ходу не приехало машин ");
        }
        for (int i = 0; i < cars.size(); i++) {
            System.out.println("Машина с номером  " + cars.get(i).number + " приехала на парковку и будет стоять  " + cars.get(i).turns + " ход(ов)");
        }
    }

    ArrayList<Car> fillParkingPlace(ArrayList<Car> parkingPlaces, ArrayList<Truck> trucksList, ArrayList<Car> carsList, int truckPlaces) {
        int misplacedCars = 0;
        int misplacedTrucks = 0;

        for (int i = 0; i < parkingPlaces.size(); i++) {
            if (parkingPlaces.get(i).turns != 0) {
                parkingPlaces.get(i).turns -= 1;
            }
        }
        int truckPlaceCounter = 0;
        for (int i = 0; i < trucksList.size(); i++) {
            int trucksCounter = -1;
            for (int j = 0; j < truckPlaces; j++) {
                if (parkingPlaces.get(j).turns == 0) {
                    parkingPlaces.get(j).turns = trucksList.get(truckPlaceCounter).turns;
                    parkingPlaces.get(j).number = trucksList.get(truckPlaceCounter).number;
                    trucksCounter = 0;
                    break;
                }
            }
            truckPlaceCounter++;
            if (trucksCounter != 0) {
                misplacedTrucks++;
            }
        }
        int placeCounter = 0;
        for (int i = 0; i < carsList.size(); i++) {
            int carsCounter = -1;
            for (int j = truckPlaces; j < parkingPlaces.size(); j++) {
                if (parkingPlaces.get(j).turns == 0) {
                    parkingPlaces.get(j).turns = carsList.get(placeCounter).turns;
                    parkingPlaces.get(j).number = carsList.get(placeCounter).number;
                    carsCounter = 0;
                    break;
                }
            }
            placeCounter++;
            if (carsCounter != 0) {
                misplacedCars++;
            }
        }

        int leftTrucksFromPlaces = 0;
        for (int i = trucksList.size() - misplacedTrucks; i < trucksList.size(); i++) {
            int trucksCounter = -1;
            for (int j = truckPlaces; j < parkingPlaces.size() - 1; j++) {
                if (parkingPlaces.get(j).turns == 0 && parkingPlaces.get(j+1).turns == 0) {
                    parkingPlaces.get(j).turns = trucksList.get(i).turns;
                    parkingPlaces.get(j+1).turns = trucksList.get(i).turns;
                    parkingPlaces.get(j).number = trucksList.get(i).number;
                    parkingPlaces.get(j+1).number = trucksList.get(i).number;
                    trucksCounter = 0;
                    break;
                }
            }
            if (trucksCounter != 0) {
                leftTrucksFromPlaces++;
            }
        }

        misplacedTrucks = leftTrucksFromPlaces;

        if (misplacedCars != 0 && misplacedTrucks != 0) {
            System.out.println();
            System.out.println( + misplacedCars + " машин(ы) и" + misplacedTrucks + " грузовиков не поместилось на парковку");

        } else if (misplacedTrucks != 0) {
            System.out.println();
            System.out.println( + misplacedTrucks + " грузовиков не поместилось на парковку");
        } else if (misplacedCars != 0) {
            System.out.println();
            System.out.println( + misplacedCars + " машин не поместилось на парковку");
        }

        return parkingPlaces;
    }

    public void getInfoAboutParking(ArrayList<Car> parkingPlaces, int parkingPlaceTrucks) {
        for (int i = 0; i < parkingPlaceTrucks; i++) {
            if (parkingPlaces.get(i).turns == 0) {
                System.out.println("На грузовом месте "+(i + 1) + " месте нет грузовика");
                continue;
            }
            System.out.println("На грузовом месте " + (i + 1) + " стоит грузовик с номером" + parkingPlaces.get(i).number + " он будет стоять " +
                    parkingPlaces.get(i).turns + " ход(ов)");
        }

        for (int i = parkingPlaceTrucks; i < parkingPlaces.size(); i++) {

            if (parkingPlaces.get(i).turns == 0) {
                System.out.println("На обычном месте " + (i + 1) + " нет машины ");
                continue;
            }
            if ((i != parkingPlaces.size() - 1) && (parkingPlaces.get(i).number==(parkingPlaces.get(i+1).number))) {
                System.out.println("На обычном месте" + (i + 1) + " и на обычном месте " + (i + 2) + " стоит грузовик с номером" + parkingPlaces.get(i).number
                        + " и он будет стоять " + parkingPlaces.get(i).turns + " ход(ов)");
                i++;
                continue;

            }
            System.out.println("На обычном месте  " + (i + 1) + " стоит машина с номером " + parkingPlaces.get(i).number + " и она будет стоять "
                    + parkingPlaces.get(i).turns+" ходов");
        }
        System.out.println();
    }

    public Parking() {
    }
    public int  Randomaizer(int max,int min){
        Random random = new Random();
        int dif=max-min;
        int randomNumber;
        randomNumber=random.nextInt(dif);
        randomNumber=randomNumber+min;
        return  randomNumber;
    }
}
