package com.company;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		while (true) {
			try{
				Scanner scanner = new Scanner(System.in);
				Parking parking = new Parking();
				System.out.println("Введите значение обычных мест");
				int amountOfCarPlaces = parking.setCarPlaces();
				System.out.println("Введите значение грузовых мест");
				int amountOfTruckPlaces = parking.setTruckPlaces();
				int amountOfAllPlaces = amountOfCarPlaces + amountOfTruckPlaces;
				ArrayList<Car> parkingPlaces = parking.setParkingPlaces(amountOfAllPlaces);
				ArrayList<Car> cars = parking.createCars(parking.setAmountOfTransportType(amountOfAllPlaces));
				ArrayList<Truck> trucks = parking.createTrucks(parking.setAmountOfTransportType(amountOfAllPlaces));
				while (true) {
					commands();
					String answer = scanner.nextLine().toLowerCase();
					switch (answer) {
						case ("1"):
							cars = parking.createCars(parking.setAmountOfTransportType(amountOfAllPlaces));
							trucks = parking.createTrucks(parking.setAmountOfTransportType(amountOfAllPlaces));
							parking.fillParkingPlace(parkingPlaces, trucks, cars, amountOfTruckPlaces);
							break;
						case ("2"):
							parking.getInfoAboutParking(parkingPlaces, amountOfTruckPlaces);
							break;
						case ("3"):
							int freePlaces = parking.checkFreePlaces(parkingPlaces);
							System.out.println("Свободно " + freePlaces + " мест");
							break;
						case ("4"):
							int occupiedPlaces = parking.checkOccupiedPlaces(parkingPlaces);
							System.out.println("Занято " + occupiedPlaces + " мест");
							break;
						case ("5"):
							parking.deleteCars(parkingPlaces);
							System.out.println("Парковка успешно очищена");
							break;
						case ("6"):
							parking.toArriveCars(cars);
							break;
						case ("7"):
							parking.toArriveTrucks(trucks);
							break;
						case ("8"):
							parking.toFindNearestFreePlace(parkingPlaces, amountOfTruckPlaces);
							break;
						default:
							System.out.println("Неизвестная команда");
							break;
					}

				}
			}catch (Exception e){
				System.out.println("Ой,вы ввели не число.попробуйте снова");
			}

		}

	}

	public static void commands(){
		System.out.println("Нажмите 1,чтобы сделать ход");
		System.out.println("Нажмите 2, чтобы узнать сколько информацию о всей парковке");
		System.out.println("Нажмите 3, чтобы узнать информацию о свободных местах");
		System.out.println("Нажмите 4, чтобы узнать информацию о занятых местаъ");
		System.out.println("Намжите 5 для очистки парковки");
		System.out.println("Нажмите 6,чтобы узнать сколько машин приехал в этот ход");
		System.out.println("Нажмите 7,чтобы узнать сколько грузовиков приехало в этот ход");
		System.out.println("Нажмите 8,чтобы узнать,когда осводится ближайшее место");

	}
}
