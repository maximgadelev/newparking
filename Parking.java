package com.company;

import java.sql.SQLOutput;
import java.util.Random;

public class Parking {
    Random random = new Random();
    Car car = new Car();
    int amountOfParkingPlace;
    int amountOfCars;

    public int[] createParkingPlaceTime(int amountOfParkingPlace) {
        int[] parkingPlaceTime = new int[this.amountOfParkingPlace];
        for (int i = 0; i < this.amountOfParkingPlace; i++) {
            parkingPlaceTime[i] = 0;
        }
        return parkingPlaceTime;
    }

    public int[] createParkingPlaceNumber(int amountOfParkingPlace) {
        int[] parkPlaceNumber = new int[this.amountOfParkingPlace];
        for (int i = 0; i < amountOfParkingPlace; i++) {
            parkPlaceNumber[i] = 0;
        }
        return parkPlaceNumber;
    }

    public int createAmountOfCars(int amountOfParkingPlace) {
        this.amountOfCars = random.nextInt(this.amountOfParkingPlace / 2 + 1);//+1,чтобы с единицой не было проблем
        return (amountOfCars);
    }

    public int[] createCarNumber(int amountOfCars) {
        int[] carNumber = new int[amountOfCars];
        for (int i = 0; i < carNumber.length; i++) {
            car.setNumber(1000 + (random.nextInt(9999 - 1000)));
            carNumber[i] = car.number;
        }
        return carNumber;
    }

    public int[] createCarTime(int amountOfCars) {
        int[] carTime = new int[amountOfCars];
        for (int i = 0; i < carTime.length; i++) {
            int time = random.nextInt(11);
            if (time == 0) {
                time = 1;
            }
            carTime[i] = time;
        }
        return carTime;
    }

    int[] fillInParkingPlaces(int[] parkingPlaceNumber, int[] parkingPlaceTime, int[] carTime, int[] carNumber) {
        int misplacedCars = 0;
        for (int i = 0; i < carTime.length; i++) {
            System.out.println("На парковку приехала машина с номером" + carNumber[i] + "она будет стоять на парковке " + carTime[i] + "ход(ов)");
        }
        if (carTime.length == 0) {
            System.out.println("На парковку не прибыло машин");
        }
        for (int i = 0; i < parkingPlaceTime.length; i++) {
            if (parkingPlaceTime[i] != 0) {
                parkingPlaceTime[i] =parkingPlaceTime[i]-1;
            }
        }

        for (int i = 0; i < carTime.length; i++) {
            for (int j = 0; j < parkingPlaceNumber.length; j++) {
                if (parkingPlaceTime[j] == 0) {
                    parkingPlaceTime[j] = carTime[i];
                    parkingPlaceNumber[j] = carNumber[i];
                    carNumber[i] = 0;
                    break;
                }
            }
            if(carNumber[i]!=0){
                misplacedCars=misplacedCars+1;
            }
        }
        if(misplacedCars!=0){
            System.out.println("!!!Парковка заполнена!!!");
            System.out.println("На парковку не поместилось " +misplacedCars+ " машин(ы)");
            int min=11;
            for (int i = 0; i <parkingPlaceTime.length ; i++) {
                if(parkingPlaceTime[i]<min){
                    min=parkingPlaceTime[i];
                }
            }
            System.out.println("!!!Ближайшее место освободиться через" + min +"ходов!!!!");
        }
        for (int i = 0; i < parkingPlaceTime.length; i++) {
            if (parkingPlaceTime[i] == 0) {
                System.out.println("На " + (i + 1) + " месте нет машины.");
                continue;
            }
            System.out.println("На " + (i + 1) + " месте  стоит машина с номером "
                    + parkingPlaceNumber[i] + " и оставшимися ходами "
                    + parkingPlaceTime[i]);
        }

        return carNumber;//-вообще не нужно
    }
}
