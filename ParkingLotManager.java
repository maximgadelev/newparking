package com.company;

import java.util.Scanner;

public class ParkingLotManager {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Parking parking = new Parking();
		Car car = new Car();
		System.out.println("Напишите количество парковочных мест:");
		while (true) {
			try {
				Scanner scanner1 = new Scanner(System.in);
				parking.amountOfParkingPlace = scanner1.nextInt();
				System.out.println("Доступны следующие команды:");
				System.out.println("Turn-сделать ход");
				System.out.println("Clear-очистить парковку");
				System.out.println("Info-информация о свободных местах,занятых,когда будет свободно ближайшее");
				int[] parkingPlaceNumber = parking.createParkingPlaceNumber(parking.amountOfParkingPlace);
				int[] parkingPLaceTime = parking.createParkingPlaceTime(parking.amountOfParkingPlace);
				while (true) {
					String answer = scanner.nextLine().toLowerCase();
					switch (answer) {
						case ("turn"):
							int amountOfCars = parking.createAmountOfCars(parking.amountOfParkingPlace);
							int[] fillInParkingPlace = parking.fillInParkingPlaces(parkingPlaceNumber, parkingPLaceTime, parking.createCarTime(parking.amountOfCars), parking.createCarNumber(amountOfCars));
							break;
						case ("clear"):
							for (int i = 0; i < parkingPlaceNumber.length; i++) {
								parkingPlaceNumber[i] = 0;
								parkingPLaceTime[i] = 0;
							}
							System.out.println("Парковка успешно очищена!Делайте ход!");
							break;
						case ("info"):
							int freePlaces = 0;
							int noFreePlaces = 0;
							for (int i = 0; i < parkingPlaceNumber.length; i++) {
								if (parkingPlaceNumber[i] == 0) {
									freePlaces = freePlaces + 1;
								} else {
									noFreePlaces = noFreePlaces + 1;
								}
							}
							int min = 11;
							for (int i = 0; i < parkingPLaceTime.length; i++) {
								if (parkingPLaceTime[i] == 1) {
									min = parkingPlaceNumber[i];
								}
								if (parkingPLaceTime[i] < 10) {
									min = parkingPLaceTime[i];
								}
							}
							System.out.println("Ближашее место осводиться через " + min + " ходов");
							System.out.println("Занято " + noFreePlaces + " мест");
							System.out.println("Свободно " + freePlaces + " мест");
							break;
						default:
							System.out.println("Неизвестная команда");
							break;
					}
				}
			} catch (Exception e) {
				System.out.println("Введите число заново");
				continue;
			}
		}
	}
}


